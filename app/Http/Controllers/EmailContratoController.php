<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\EmailContrato;
use App\Mail\EmailContratoValidado;
use Illuminate\Support\Facades\Mail;
use PDF;
use App\Models\Parceiro;
use App\Models\Contrato;


class EmailContratoController extends Controller
{
    public function enviarContrato(Request $request){

        $parceiro = new Parceiro();

        $parceiro->uuidParceiroContrato = $request->uuidParceiroContrato;
        $parceiro->uuidParceiro = $request->uuidParceiro;
        $parceiro->parceiroNome = $request->parceiroNome;
        $parceiro->parceiroEmail = $request->parceiroEmail;
        $parceiro->parceiroNacionalidade = $request->parceiroNacionalidade;
        $parceiro->parceiroEstadoCivil = $request->parceiroEstadoCivil;
        $parceiro->parceiroRgNumero = $request->parceiroRgNumero;
        $parceiro->parceiroRgOrgao = $request->parceiroRgOrgao;
        $parceiro->parceiroDocumentoCpfCnpj = $request->parceiroDocumentoCpfCnpj;
        $parceiro->parceiroEnderecoComplemento = $request->parceiroEnderecoComplemento;
        $parceiro->parceiroCidade = $request->parceiroCidade;
        $parceiro->parceiroLogradouro = $request->parceiroLogradouro;
        $parceiro->parceiroEnderecoNum = $request->parceiroEnderecoNum;
        $parceiro->parceiroCep = $request->parceiroCep;
        $parceiro->tipoParceiro = $request->tipoParceiro;
        $parceiro->parceiroCodigo = $request->parceiroCodigo;

        $out = new \Symfony\Component\Console\Output\ConsoleOutput();
        $out->writeln($parceiro->parceiroCidade);
        $out->writeln($parceiro->parceiroNome);
        $out->writeln($parceiro->parceiroEmail);
        $out->writeln($parceiro->parceiroNacionalidade);
        $out->writeln($parceiro->parceiroEstadoCivil);
        $out->writeln($parceiro->parceiroEnderecoNum);
        $out->writeln($parceiro->parceiroCep);
        $out->writeln($parceiro->tipoParceiro);
        $out->writeln($parceiro->uuidParceiro);

        if($parceiro->tipoParceiro == 'PESSOA_FISICA' || $parceiro->tipoParceiro == 'PARCEIRO_INTERNO'){

            $pdf = PDF::loadView('pdf/contratoPessoaFisica', ['parceiro' => $parceiro])->setPaper('a4')->save('../storage/app/contratos/'.$parceiro->parceiroEmail.'.pdf');

        }else if($parceiro->tipoParceiro == 'PESSOA_JURIDICA'){

            //EM CASO DE SER PESSOA JURIDICA, PRECISA DESSES CAMPOS
            $parceiro->responsavelNome = $request->responsavelNome;
            $parceiro->responsavelCpf = $request->responsavelCpf;
            $parceiro->responsavelLogradouro = $request->responsavelLogradouro;
            $parceiro->responsavelEnderecoNum = $request->responsavelEnderecoNum;
            $parceiro->responsavelEnderecoComplemento = $request->responsavelEnderecoComplemento;
            $parceiro->responsavelCidade = $request->responsavelCidade;
            $parceiro->responsavelCep = $request->responsavelCep;

            $pdf = PDF::loadView('pdf/contratoPessoaJuridica', ['parceiro' => $parceiro])->setPaper('a4')->save('../storage/app/contratos/'.$parceiro->parceiroEmail.'.pdf');
        }

            return Mail::send(new EmailContrato($parceiro));
    }

    public function contratoValidado(Request $request){
        $contrato = new Contrato();

        $contrato->parceiroNome = $request->parceiroNome;
        $contrato->parceiroEmail = $request->parceiroEmail;

        return Mail::send(new EmailContratoValidado($contrato));
    }
}
