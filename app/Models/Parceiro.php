<?php

namespace App\Models;


class Parceiro
{
    public $uuidParceiroContrato;
    public $uuidParceiro;
    public $parceiroNome;
    public $tipoParceiro;
    public $parceiroRgNumero;
    public $parceiroRgOrgao;
    public $parceiroDocumentoCpfCnpj;
    public $parceiroNacionalidade;
    public $parceiroEstadoCivil;
    public $parceiroCep;
    public $parceiroCidade;
    public $parceiroEnderecoComplemento;
    public $parceiroEmail;
    public $parceiroCodigo;

    //CASO SEJA PESSOA JURIDICA
    public $responsavelNome;
    public $responsavelCpf;
    public $responsavelLogradouro;
    public $responsavelEnderecoNum;
    public $responsavelEnderecoComplemento;
    public $responsavelCidade;
    public $responsavelCep;
}
