<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Parceiro;

class EmailContrato extends Mailable
{
    use Queueable, SerializesModels;

    private $parceiro;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Parceiro $parceiro)
    {
        $this->parceiro = $parceiro;

    }



    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $this->subject("Formalização de Contratos");
        $this->from(env('MAIL_USERNAME'), "Formalização de Contratos");
        $this->to($this->parceiro->parceiroEmail, $this->parceiro->parceiroNome);
        $this->attach(storage_path("app/contratos/".$this->parceiro->parceiroEmail.".pdf"));

        return $this->markdown('email/email', ['parceiro' => $this->parceiro]);

    }
}
