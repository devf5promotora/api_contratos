<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Contrato;


class EmailContratoValidado extends Mailable
{
    use Queueable, SerializesModels;

    private $contrato;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contrato $contrato)
    {
        $this->contrato = $contrato;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject("Formalização de Contratos");
        $this->from(env('MAIL_USERNAME'), "Formalização de Contratos - Validação de contrato");
        $this->to($this->contrato->parceiroEmail, $this->contrato->parceiroNome);

        return $this->markdown('email/emailContratoValidado', ['contrato' => $this->contrato]);

    }
}
