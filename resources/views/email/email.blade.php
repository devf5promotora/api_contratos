@component('mail::message')

<style>

/* CSS */
.button-3 {
  appearance: none;
  background: #55BEA4;
  border: 1px solid rgba(27, 31, 35, .15);
  border-radius: 6px;
  box-shadow: rgba(27, 31, 35, .1) 0 1px 0;
  box-sizing: border-box;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  font-family: -apple-system,system-ui,"Segoe UI",Helvetica,Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji";
  font-size: 14px;
  font-weight: 600;
  line-height: 20px;
  padding: 6px 16px;
  position: relative;
  text-align: center;
  text-decoration: none;
  user-select: none;
  -webkit-user-select: none;
  touch-action: manipulation;
  vertical-align: middle;
  white-space: nowrap;
}

.button-3:focus:not(:focus-visible):not(.focus-visible) {
  box-shadow: none;
  outline: none;
}

.button-3:hover {
  background-color: #2c974b;
}

.button-3:focus {
  box-shadow: rgba(46, 164, 79, .4) 0 0 0 3px;
  outline: none;
}

.button-3:disabled {
  background-color: #94d3a2;
  border-color: rgba(27, 31, 35, .1);
  color: rgba(255, 255, 255, .8);
  cursor: default;
}

.button-3:active {
  background-color: #298e46;
  box-shadow: rgba(20, 70, 32, .2) 0 1px 0 inset;
}
</style>


<img src="http://f5suporte.tk/imagens-api/header-f5.png" alt="">


<div style="padding: 2%">
<h1>CONTRATO DE PRESTAÇÃO DE SERVIÇOS AUTÔNOMOS.</h1>


<p>anexo com pdf do contrato.</p>


<center>
<a href="http://192.168.0.179:3307/solicitacao-status-contrato/aceitar-contrato/{{$parceiro->uuidParceiroContrato}}/{{$parceiro->parceiroCodigo}}" class="button-3 role="button">Aceitar Contrato</a>
</center>

<br>


<center>
<a href="http://192.168.0.179:3307/solicitacao-status-contrato/negar-contrato/{{$parceiro->uuidParceiroContrato}}/{{$parceiro->parceiroCodigo}}" class="button-3 role="button">Rejeitar Contrato</a>
</center>


</div>

@endcomponent
