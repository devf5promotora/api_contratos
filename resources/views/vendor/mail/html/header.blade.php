<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{url('assets/img/header-f5.png')}}" class="logo" alt="f5 Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
