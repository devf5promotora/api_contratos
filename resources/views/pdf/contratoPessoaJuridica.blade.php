<style>
p,
li {
    text-align: justify;
}

,
img {
    width: 70px;
    height: 70px;
}
</style>
<br>


<center>
    <img src="http://f5suporte.tk/imagens-api/f5maislogo.jpeg" alt="">

</center>


<h2>CONTRATO DE PRESTAÇÃO DE SERVIÇOS AUTÔNOMOS.</h2>


<br>

<p>CONTRATANTE: F5 PROMOTORA E SERVICOS CADASTRAIS
    EIRELI - ME, pessoa jurídica de direito privado, com endereço na
    Praça 23 de Junho, no 10, sala 13, CEP 61.760-000, Eusébio, Ceará,
    inscrita no CNPJ sob o n. 13.041.159/0001-04, neste ato por seu
    representante legal, infra assinado.</p>

<br>

<p>


    CONTRATADO: {{ $parceiro->parceiroNome ?? '___________________________________________________________' }} CNPJ/M
    F n.º {{ $parceiro->parceiroDocumentoCpfCnpj ?? '___.____.__/________'}}, Endereço na Rua/Av:
    {{ $parceiro->parceiroLogradouro ?? '_______________'}} Nº {{ $parceiro->parceiroEnderecoNum ?? '______'}} na
    Cidade de {{ $parceiro->parceiroCidade ?? '_______________'}}, CEP {{ $parceiro->parceiroCep ?? '__.____-___'}},
    Representado por
    {{ $parceiro->responsavelNome ?? '__________________________________________________'}}, CPF:
    {{ $parceiro->responsavelCpf ?? '___.___.___-__'}} , Endereço
    na Rua/Av {{ $parceiro->responsavelLogradouro ?? '__________________________'}}, Nº
    {{ $parceiro->responsavelEnderecoNum ?? '______'}}, complemento
    {{ $parceiro->responsavelEnderecoComplemento ?? '____________'}},
    Cidade {{ $parceiro->responsavelCidade ?? '________________'}}, CEP:
    {{ $parceiro->responsavelCep ?? '___.____-___'}}.

</p>

<br>

<p>As partes acima mencionadas e qualificadas resolvem de pleno e comum
    acordo, celebrar o presente CONTRATO DE PRESTAÇÃO DE SERVIÇOS
    AUTÔNOMOS, mediante as cláusulas e condições a seguir avençadas:</p>

<br>


<center>
    <h3>DO OBJETO:</h3>
</center>
<p>
    Clausula Primeira: O presente instrumento tem como objeto a prestação
    de serviços autônomos, na coleta de documentos que possibilitem a
    contratação de empréstimos consignados e seguros, pelo(a)
    CONTRATADO(A), no sentido de viabilizar contato com clientes
    interessados nos produtos oferecidos pelas instituições financeiras (IF)
    ou afins com quem a CONTRATANTE mantenha relação de
    CORRESPONDENTE, na modalidade de empréstimo consignado, e/ou,
    qualquer outro produto do portfólio da Instituição, sendo
    responsabilidade do (a) CONTRATADO(a) cumprir a formalização da(s)
    operação(ões), obedecendo aos roteiros operacionais de cada instituição
    financeira.</p>

<br>

<p>Parágrafo Primeiro:
    A prestação dos serviços ora contratados não se dará em caráter de
    exclusividade, ficando o (a) CONTRATADO (A), livre para firmar
    contratos com objeto semelhante ao previsto neste instrumento com
    outros correspondentes, promotores de crédito, instituições
    financeiras e assemelhados, bem como, comercializar ou divulgar
    produtos sem o prévio consentimento do CONTRATANTE.</p>

<br>

<!-- PAGINA 2 -->


<p>Parágrafo Segundo:
    Os serviços ora contratados serão prestados pelo CONTRATADO(A), no
    âmbito do Estado do ( identificar o Estado de atuação), podendo a
    CONTRATANTE a seu livre critério, ampliar ou reduzir essa área de atuação,
    cabendo ao (à) CONTRATADO (A), sujeitar-se aos parâmetros geográficos
    impostos pela CONTRATANTE.</p>

<br>

<center>
    <h3>DAS OBRIGAÇÕES E RESPONSABILIDADE DA CONTRATADA</h3>
</center>

<h4>CLAUSULA SEGUNDA:</h4>


<ul>

    <li>Fornecer toda a mão-de-obra especializada necessária à execução
        e supervisão da intermediação objeto do presente Contrato.</li>

    <br>

    <li>Se responsabilizar de forma integral civil, administrativa e
        criminalmente por todos os usuários e senhas disponibilizadas
        pela CONTRATANTE por serem intransferíveis a terceiros,
        utilizando-se de boa índole e moral ao utilizá-las, ficando como fiel
        depositário(a) de tais senhas, respondendo por todo e qualquer
        dano que sua má utilização por si ou por terceiros venha causar, o
        que com a assinatura do presente declara aceitar.</li>

    <br>


    <li>Responder pelo comportamento e eficiência do pessoal sob a sua
        direção;</li>

    <br>


    <li>Manter a CONTRATANTE a salvo de responsabilidade sobre
        qualquer processo, ações administrativas ou judiciárias surgidas
        em decorrência de omissão, negligência ou imperícia quando do
        preenchimento e remessa de fichas cadastrais e outros
        documentos para a CONTRATANTE.</li>

    <br>


    <li>Ressarcir a CONTRATANTE de todas as despesas/custos
        referentes a qualquer tipo de contencioso (fiscal, civil e trabalhista)
        que venha a fazer parte ou que sejam decorrentes de quaisquer
        atos da CONTRATADA, inclusive aqueles relacionados à operação
        de crédito, como TED ́s bancárias para disponibilização de recursos
        nas contas dos clientes que venham a ser descontadas da
        CONTRATADA.</li>

    <br>


    <li>Correrão por conta e a cargo exclusivo da CONTRATADA, as
        despesas necessárias a presente prestação de serviços, inclusive
        com locomoção, alimentação, despesas com telefones, postagens
        em correio, fax e materiais de escritório, bem como, todos os impostos, taxas e quaisquer encargos decorrentes
        da presente
        prestação de serviços, obrigando-se pelos seus recolhimentos nas
        datas devidas.</li>

    <br>

    <!-- pagina 3 -->

    <li>Responsabilidade por todos os serviços administrativos
        decorrentes desta avença, responsabilizando-se pelos serviços de
        confecção de planilhas, checagem de fichas cadastrais, consulta aos
        órgãos de proteção ao crédito e todos os demais afazeres inerentes
        à boa qualidade dos serviços prestados.</li>

    <br>

    <li>Responsabilidade decorrente dos atos, omissivos ou comissivos,
        praticados pelos representantes e prepostos, acerca de fraudes,
        falhas e/ou defeitos relativos à prestação dos serviços, devendo o
        prejuízo ser suportado pela CONTRATADA, autorizando a
        CONTRATANTE a estornar tais valores de suas comissões, bem
        como a dedução de créditos porventura existentes junto a
        CONTRATANTE, ficando esta autorizada à inclusão da
        CONTRATADA nos órgãos de proteção ao crédito (SPC/SERASA),
        caso após notificada para pagar o débito não o fizer no prazo
        estipulado.</li>

    <br>

    <li>Executar, se for o caso e a exclusivo critério CONTRATANTE, além
        dos itens antes relacionados, todos os demais procedimentos que
        se fizerem necessários à espécie, porém de conformidade com a
        regularidade e normatização vigente e competente, sobretudo
        preservando e respeitando a identidade comercial e a atividade
        peculiar da CONTRATADA.</li>

    <br>

    <li>Remeter os contratos solicitados pelo CONTRATANTE, dentro do
        prazo estabelecido pelo Banco que será responsável pelo
        pagamento do contrato, podendo sofrer estorno do valor da
        comissão, se assim não o fizer. Não havendo o cumprimento dos
        prazos, a CONTRATADA responderá pelos danos e cominações
        legais referentes ao não envio destes, bem como, se responsabiliza
        pela não prestação de informações relativas aos contratos e aos
        tomadores quando solicitadas.</li>

    <br>

    <li>Sem prejuízo das demais obrigações e responsabilidades legais e
        contratuais, o(a) CONTRATADO (A), assume as responsabilidades
        civil, penal, tributária, trabalhista ou de qualquer outra natureza,
        decorrentes dos documentos e informações que, no desenvolvimento
        de suas atividades, fornecer à CONTRATANTE.</li>



    <!-- pagina 4 -->

    <li>responsabiliza-se pela perfeita e correta execução de suas
        atividades, pela autenticidade dos documentos encaminhados que
        derem origem aos empréstimos ou financiamentos, bem como, pela
        perfeita formalização desses. Sendo ainda o mesmo responsável por
        seus atos, de seus funcionários e/ou de seus prepostos, devendo
        ainda indenizar prejuízos decorrentes da não observância do
        disposto na presente.
    </li>

    <br>

</ul>

<center>
    <h3>DA INEXISTÊNCIA DE VINCULO TRABALHISTA</h3>
</center>


<p>CLAUSULA TERCEIRA: O (A) CONTRATADO(A), assim como os seus
    agentes/trabalhadores, sejam empregados, autônomos ou de qualquer outra
    classe laboral, que lhe prestem serviços, não manterão qualquer espécie de
    vínculo empregatício junto à CONTRATANTE, não cabendo ao (à)
    CONTRATADO(A), tampouco a qualquer de seus colaboradores, diretos ou
    indiretos, empregados ou não, o direito de requerer da CONTRATANTE
    pagamento de qualquer verba ou indenização, tanto no âmbito
    trabalhista, previdenciário, securitário, tributário ou de depósitos fundiários
    em suas contas vinculadas.</p>

<br>

<p>Parágrafo Único - Caso a CONTRATANTE seja demandada em juízo ou fora
    dele, a fim de compor demanda judicial em que figure como autor, qualquer
    empregado ou prestador de serviços do(a) CONTRATADO(A), e havendo
    condenação pecuniária a seu desfavor, lhe será cabido o competente direito
    de retenção de valores de comissão, e/ou direito de regresso contra o(a)
    CONTRATADO(A), que se obriga, pelo presente Instrumento, ao
    pagamento integral da quantia objeto de condenação judicial.
    A CONTRATADA deverá abster-se de receber, em nome da
    CONTRATANTE, qualquer tipo de correspondência, intimação, citação,
    extrajudicial, devendo tão somente declinar o endereço da
    CONTRATANTE ao respectivo interessado, sob pena de responderem
    por todos os prejuízos que a CONTRATANTE experimenta em
    decorrência do descumprimento do ora ajustado.</p>

<br>
<center>
    <h3>DO PRAZO</h3>
</center>

<p>CLÁUSULA QUARTA: Este Contrato em vigor a partir de sua assinatura, e
    vigerá por tempo indeterminado.</p>

<br>

<center>
    <h3>DAS VEDAÇÕES AO CONTRATADO (A)</h3>
</center>


<p>CLAUSULA QUINTA: Considerando-se que a prestação de serviços de
    responsabilidade da CONTRATADA se restringem, exclusivamente, aos
    procedimentos relacionados nas cláusulas retro, fica desde já expressa,
    terminantemente e impreterivelmente vedado à ela, a prática por si ou
    através de seus agentes (representantes, funcionários, prepostos ou
    franqueados) dos seguintes atos:
</p>

<br>


<ul>
    <li>
        Substabelecer este contrato a terceiros, total ou parcialmente, sem
        prévia e expressa autorização da CONTRATANTE;
    </li>

    <br>

    <li>
        Promover qualquer tipo ou espécie de publicidades, seja qual for o
        canal de divulgação, fora dos padrões autorizados pela
        CONTRATANTE, de quaisquer produtos do mesmo, inclusive
        daquele objeto deste contrato, sem a sua prévia e expressa
        autorização;
    </li>

    <br>


    <li>
        Receber quaisquer importâncias a título de pagamento das
        prestações dos empréstimos, totais ou parciais, dos respectivos
        tomadores, não, explicitamente, previsto contratualmente e fora
        dos padrões normais da CONTRATANTE, tudo em conformidade
        com o roteiro operacional acordado;
    </li>

    <br>

    <li>
        Emitir, a seu favor, carnês ou títulos relativos às operações da
        CONTRATANTE, por ela intermediadas;
    </li>

    <br>

    <li>
        Prestar aval ou qualquer outro tipo de garantia nas operações
        liberadas pela CONTRATANTE;
    </li>

    <br>

    <li>Cobrar por iniciativa própria, dos tomadores de empréstimos, para
        viabilização de operações com CONTRATANTE, qualquer valor,
        taxa, comissões, despesas emolumentos cartorários, tarifas,
        despesas de administração, a implantação de contratos,
        preenchimento de formulários, obtenção de documentos e
        quaisquer outros, sejam de que natureza forem, ou qualquer tarifa
        relacionada com a prestação dos serviços objeto deste contrato;</li>

    <br>

    <li>Captar recursos em nome da CONTRATANTE;</li>

    <br>

    <li>
        Exigir dos tomadores de empréstimos, quaisquer importâncias a
        título de encargos moratórios, fora dos parâmetros legais adotados
        pela CONTRATANTE;
    </li>
</ul>

<br>

<h3>PARÁGRAFO ÚNICO</h3>


<p>A subcontratação dos serviços será permitida somente previa e
    expressamente, contudo ficará sob responsabilidade total da
    CONTRATADA, no aspecto geral do termo, não atingindo a
    CONTRATANTE, por inexistência de qualquer vinculo contratual, com
    o subcontratado, em decorrência desta avença.</p>

<br>

<center>
    <h3>DA REMUNERAÇÃO</h3>
</center>


<p>CLÁUSULA SEXTA – Pela prestação dos serviços ora contratados, a
    CONTRATANTE pagará ao(à) CONTRATADO(A), comissão sobre os
    contratos aprovados, nos patamares constantes da planilha confeccionada
    após o recebimento e a conferência da documentação, em conformidade
    com a(s) tabela(s) apresentadas pela(s) Instituição(ões) Financeira(s),
    da(s) qual(is) a CONTRATANTE é Correspondente/Parceira.</p>


<h3>PARÁGRAFO PRIMEIRO</h3>

<p>A CONTRATADA deverá remeter os contratos solicitados pelo
    CONTRATANTE, dentro do prazo estabelecido pelo Banco que será
    responsável pelo pagamento do contrato, podendo sofrer estorno do
    valor da comissão, se assim não o fizer. Não havendo o cumprimento dos
    prazos, a CONTRATADA responderá pelos danos e cominações legais
    referentes ao não envio destes, bem como, se responsabiliza pela não
    prestação de informações relativas aos contratos e aos tomadores
    quando solicitadas, prestação de contas da produção alcançada, uso
    indevido que fizer alusivo às senhas confiadas, dentre quaisquer outras
    ações que, direta ou indiretamente, prejudique o escorreito
    procedimento de efetivação de empréstimos consignados.</p>

<br>

<p>Havendo discordância de informações, a CONTRATANTE pagará apenas
    a parte incontroversa e, caso devido, pagará a diferença. Se houver, na
    semana seguinte.</p>

<br>

<center>
    <h3>DAS RESPONSABILIDADES</h3>
</center>


<p>CLÁUSULA SÉTIMA: Sem prejuízo das demais obrigações e
    responsabilidades legais e contratuais, o(a) CONTRATADO (A), assume as
    responsabilidades civil, penal, tributária, trabalhista ou de qualquer outra
    natureza, decorrentes dos documentos e informações que, no
    desenvolvimento de suas atividades, fornecer à CONTRATANTE.</p>

<br>

<p>A CONTRATADA responsabiliza-se pela perfeita e correta execução de
    suas atividades, pela autenticidade dos documentos encaminhados que
    derem origem aos empréstimos ou financiamentos, bem como, pela
    perfeita formalização desses. Sendo ainda o mesmo responsável por seus
    atos, de seus funcionários e/ou de seus prepostos, devendo ainda
    indenizar prejuízos decorrentes da não observância do disposto na
    presente.</p>

<br>

<center>
    <h3>DO ACOMPANHAMENTO DOS SERVIÇOS</h3>
</center>



<p>CLAUSULA OITAVA: A CONTRATANTE poderá acompanhar e fiscalizar a
    execução da presente prestação de serviços, por pessoas de sua indicação,
    devendo a CONTRATADA permitir-lhe o acesso a toda a documentação
    pertinente e exigida pelas aludidas pessoas, sem que, com isso, exima a
    CONTRATADA de suas responsabilidades, de seus funcionários ou dos
    seus agentes e ou, subcontratados, pelo cumprimento de suas obrigações,
    no tocante ao objeto do presente contratado. Caso seja constatada alguma
    irregularidade, a CONTRATADA se obriga por si e ou pelas pessoas por
    elas contratadas, pelo seu imediato saneamento, dentro de, no máximo
    10(dez) dias contados da notificação que, para este fim, lhe fizer a
    CONTRATANTE.</p>


<br>

<center>
    <h3>DA CONFIDENCIALIDADE E SIGILO</h3>
</center>

<p>CLÁUSULA NONA: Cada uma das Partes obriga-se a manter em sigilo e respeitar
    a confidencialidade dos dados e informações, verbais ou escritas, relativos às
    operações e negócios da outra Parte (incluindo, sem limitação, todos os segredos
    e/ou informações financeiras, operacionais, econômicas, técnicas e jurídicas), dos
    contratos, pareceres e outros documentos, bem como de quaisquer cópias ou
    registros dos mesmos, contidos em qualquer meio físico a que a referida Parte
    obrigada tiver acesso em virtude deste Contrato (as “Informações
    Confidenciais”), ficando desde já estabelecido que (i) as Informações
    Confidenciais somente poderão ser divulgadas a seus sócios, administradores,
    procuradores, consultores, prepostos e empregados, presentes ou futuros, que
    precisem ter acesso às Informações Confidenciais em virtude do cumprimento
    das obrigações estabelecidas neste Contrato (os “Representantes”), e (ii) que a
    divulgação a terceiros, direta ou indiretamente, no todo ou em parte, isolada ou
    conjuntamente, no Brasil ou no exterior, por qualquer meio, de quaisquer
    Informações Confidenciais dependerá de prévia e expressa autorização, por
    escrito, das demais Partes.</p>

<br>

<p>Parágrafo primeiro: As Partes comprometem-se a não utilizar quaisquer das
    Informações Confidenciais em proveito próprio ou de quaisquer terceiros e
    responsabilizam-se pela violação das obrigações previstas nesta Cláusula por
    parte de quaisquer dos Representantes.</p>

<br>

<p>Parágrafo segundo: Caso qualquer das Partes e/ou quaisquer de seus
    Representantes sejam obrigados, em virtude de lei, de decisão judicial ou por
    determinação de qualquer autoridade governamental, a divulgar quaisquer das
    Informações Confidenciais, tal Parte, sem prejuízo do atendimento tempestivo à
    determinação legal ou administrativa, deverá, exceto no caso em que seja
    impedida em decorrência de determinada ordem judicial ou norma, comunicar
    imediatamente as outras Partes a respeito dessa obrigação, de modo que as
    Partes, se possível e em mútua cooperação, possam intentar as medidas cabíveis,
    inclusive judiciais, para preservar as Informações Confidenciais. Caso as medidas
    tomadas para preservar as Informações Confidenciais não tenham êxito, deverá
    ser divulgada somente a parcela das Informações Confidenciais estritamente
    necessária à satisfação do dever legal e/ou cumprimento de ordem judicial ou de
    qualquer autoridade competente de divulgação das informações.
</p>

<br>

<p>Parágrafo terceiro: Excluem-se do compromisso de confidencialidade aqui
    previsto as informações: (i) disponíveis para o público de outra forma que não
    pela divulgação das mesmas por qualquer das Partes e/ou por qualquer de seus
    Representantes; e (ii) que comprovadamente já eram do conhecimento de uma
    ou de todas as Partes e/ou de qualquer de seus Representantes antes da referida
    Parte Obrigada ou seus Representantes terem acesso em função deste Contrato.</p>

<br>

<p>Parágrafo quarto O dever de confidencialidade previsto nesta Cláusula
    remanescerá ao término deste Contrato pelo prazo de 15 (quinze) anos, estando
    seu descumprimento sujeito ao disposto neste Contrato a qualquer tempo
    durante a vigência do prazo ora referido, inclusive após a extinção ou a resolução
    deste Contrato.</p>

<br>

<center>
    <h3>DOS PROCEDIMENTOS DE PREVENÇÃO À PRÁTICA DE ATOS ILÍCITOS</h3>
</center>

<p>C LÁUSULA DÉCIMA : As Partes, por si e por seus administradores, diretores,
    empregados e agentes, obrigam-se a: (i) conduzir suas práticas comerciais de
    forma ética e em conformidade com os preceitos legais aplicáveis; (ii) notificar
    imediatamente a outra parte se tiverem conhecimento ou suspeita de qualquer
    conduta que constitua ou possa constituir prática de suborno ou corrupção
    referente à negociação, conclusão ou execução deste Contrato, e declaram, neste
    ato, que não realizaram e nem realizarão qualquer pagamento, nem forneceram
    ou fornecerão benefícios ou vantagens a quaisquer autoridades governamentais,
    ou a consultores, representantes, parceiros ou terceiros a elas ligados, com a
    finalidade de influenciar qualquer ato ou decisão da administração pública ou
    privada ou assegurar qualquer vantagem indevida obter ou impedir negócios ou
    auferir qualquer benefício indevido.</p>

<br>

<center>
    <h3>POLÍTICA DE PRIVACIDADE E PROTEÇÃO DE DADOS</h3>
</center>

<p>CLÁUSULA DÉCIMA PRIMEIRA: Em cumprimento à Lei Geral de Proteção de
    Dados (LGPD com a redação dada pela Lei no 13.853/2019, o CONTRATATANTE
    se obriga a respeitar a privacidade do CONTRATADO, comprometendo-se a
    proteger e manter em sigilo todos os dados pessoais fornecidos pelo mesmo em
    função deste contrato, salvo os casos em que seja obrigado, por autoridades
    públicas, a revelar tais informações a terceiros.</p>

<br>

<p>Parágrafo primeiro: Nos termos do Art. 7o, inc. VI, da LGPD, o CONTRATANTE
    está autorizado a realizar o tratamento de dados pessoais do CONTRATADO, e com
    base no art. 10o, inc. I, da LGPD, ostenta legítimo interesse em armazenar, acessar,
    avaliar, modificar, transferir e comunicar, sob qualquer forma e por tempo
    indeterminado todos e quaiquer contratos, e-mails, cadastros, cartas e demais
    documentações relativas ao objeto desta contratação.</p>

<br>

<p>Parágrafo segundo: Tal operação de tratamento de dados é e sempre será
    realizada unicamente em apoio e promoção às atividades técnicas e intelectuais
    desenvolvidas internamente pelo CONTRATANTE, em especial para fins de
    comprovação dos termos constantes no presente instrumento, resguardando
    todos os direitos inerentes às partes.</p>

<br>

<center>
    <h3>DO PRAZO</h3>
</center>

<p>CLÁUSULA DÉCIMA SEGUNDA: O presente Contrato vigorará por prazo
    indeterminado, podendo, entretanto, ser resilido a qualquer tempo
    mediante comunicação de uma parte a outra, através de correspondência
    protocolizada, enviada com 24 (vinte quatro) horas de antecedência,
    mantida, entretanto, a remuneração devida à CONTRATADA pelas
    operações de empréstimos contratadas até a data de recepção da aludida
    comunicação.</p>

<br>

<center>
    <h3>DAS DISPOSIÇÕES GERAIS</h3>
</center>

<p>CLÁUSULA DÉCIMA TERCEIRA: O presente Instrumento é realizado em
    caráter irrevogável, irretratável e intransferível, o qual obriga as partes a
    cumpri-lo, a qualquer título, bem como seus herdeiros e sucessores.</p>

<br>

<p>CLÁUSULA DÉCIMA QUARTA: A tolerância ou transigência de qualquer
    das Partes não implicará novação, perdão, renúncia, alteração ou
    modificação do ora pactuado, sendo o evento ou omissão considerado, para
    todos os fins de direito, como mera liberalidade da Parte que transigiu,
    anuiu ou não exigiu o cumprimento da obrigação, não implicando, todavia, a
    renúncia do direito de exigir o cumprimento das obrigações aqui pactuadas,
    a qualquer tempo.</p>

<br>

<p>CLÁUSULA DÉCIMA QUINTA: Toda e qualquer modificação, alteração ou
    aditamento ao presente Contrato somente será válido se feito por escrito e
    devidamente assinado pelas Partes</p>

<br>

<p>CLÁUSULA DÉCIMA SEXTA: Caso qualquer uma das cláusulas do presente
    Contrato venha a ser anulada ou declarada nula, no todo ou em parte, por
    qualquer motivo, as demais cláusulas, não afetadas pelo defeito ou nulidade,
    continuarão em pleno vigor a menos que o objeto deste Contrato seja afetado.</p>

<br>

<p>CLÁUSULA DÉCIMA SÉTIMA: Os termos deste CONTRATO, quando couber,
    estarão sujeitos a execução específica, conforme disposto na legislação,
    para o que as PARTES reconhecem constituir o presente título executivo
    extrajudicial para todos os fins dos referidos artigos.</p>

<br>

<center>
    <h3>DO FORO</h3>
</center>

<p>CLÁUSULA DÉCIMA OITAVA:. As partes elegem o foro da Comarca da
    Cidade de Russas, Estado do Ceará, para dirimir qualquer dúvida ou julgar
    qualquer litígio oriundo da presente cessão, com renúncia expressa a
    qualquer outro, por mais privilegiado que seja ou venha a ser.</p>

<br>

<p>E, POR ESTAREM JUSTAS E CONTRATADAS, as partes assinam o presente
    instrumento em 2 vias, de iguais teor e forma, na presença de duas
    testemunhas.</p>

<br>

<center>
    <p>_______________________, ____,___ de ____________de 20____.</p>
</center>

<br>

<hr>

<strong>
    <center>
        {{ $parceiro->parceiroNome ?? 'RAZÃO SOCIAL<br>CNPJ/MF:'}}
    </center>
</strong>

<hr>

<center>
    <h3>F5 PROMOTORA E SERVIÇOS CADASTRAIS EIRELI ME
        CNPJ/MF: 13.014.159/0001-04</h3>
</center>

<br>
<p>TESTEMUNHAS</p>

<br>

<p>1. _____________________________<br> Nome: <br> CPF: </p>

<br>

<p>2. _____________________________<br> Nome: <br> CPF: </p>

<br>


<center>
    <p>Termo válido através de telefone e email pessoal.</p>
</center>